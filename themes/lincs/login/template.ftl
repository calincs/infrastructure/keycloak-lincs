<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayRequiredFields=false>
<!DOCTYPE html>
<html class="${properties.kcHtmlClass!}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <title>${msg("loginTitle",(realm.displayName!''))}</title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />
    <#if properties.stylesCommon?has_content>
        <#list properties.stylesCommon?split(' ') as style>
            <link href="${url.resourcesCommonPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
</head>

<body class="${properties.kcBodyClass!}">
<nav id="kc-navbar" class="${properties.kcNavClass!}">
    <div class="${properties.kcLeftNavClass!}" >
    <span>LINCS Login</span>
    </div>

    <div class="${properties.kcRightNavClass!}" >
        <a href="https://portal.lincsproject.ca" target="_blank">Portal<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) --><path fill="currentColor" d="M432,320H400a16,16,0,0,0-16,16V448H64V128H208a16,16,0,0,0,16-16V80a16,16,0,0,0-16-16H48A48,48,0,0,0,0,112V464a48,48,0,0,0,48,48H400a48,48,0,0,0,48-48V336A16,16,0,0,0,432,320ZM488,0h-128c-21.37,0-32.05,25.91-17,41l35.73,35.73L135,320.37a24,24,0,0,0,0,34L157.67,377a24,24,0,0,0,34,0L435.28,133.32,471,169c15,15,41,4.5,41-17V24A24,24,0,0,0,488,0Z"/></svg></a>
        <a href="https://gitlab.com/calincs/" target="_blank">GitLab<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) --><path fill="currentColor" d="M432,320H400a16,16,0,0,0-16,16V448H64V128H208a16,16,0,0,0,16-16V80a16,16,0,0,0-16-16H48A48,48,0,0,0,0,112V464a48,48,0,0,0,48,48H400a48,48,0,0,0,48-48V336A16,16,0,0,0,432,320ZM488,0h-128c-21.37,0-32.05,25.91-17,41l35.73,35.73L135,320.37a24,24,0,0,0,0,34L157.67,377a24,24,0,0,0,34,0L435.28,133.32,471,169c15,15,41,4.5,41-17V24A24,24,0,0,0,488,0Z"/></svg></a>
    </div>
</nav>
<div class="${properties.kcLoginClass!}">
    <div id="kc-header" class="${properties.kcHeaderClass!}">
        <div id="kc-header-wrapper"
             class="${properties.kcHeaderWrapperClass!}">${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc}</div>
    </div>
    <div class="${properties.kcFormCardClass!}">
        <header class="${properties.kcFormHeaderClass!}">
            <#if realm.internationalizationEnabled  && locale.supported?size gt 1>
                <div class="${properties.kcLocaleMainClass!}" id="kc-locale">
                    <div id="kc-locale-wrapper" class="${properties.kcLocaleWrapperClass!}">
                        <div id="kc-locale-dropdown" class="${properties.kcLocaleDropDownClass!}">
                            <a href="#" id="kc-current-locale-link">${locale.current}</a>
                            <ul class="${properties.kcLocaleListClass!}">
                                <#list locale.supported as l>
                                    <li class="${properties.kcLocaleListItemClass!}">
                                        <a class="${properties.kcLocaleItemClass!}" href="${l.url}">${l.label}</a>
                                    </li>
                                </#list>
                            </ul>
                        </div>
                    </div>
                </div>
            </#if>
        <#if !(auth?has_content && auth.showUsername() && !auth.showResetCredentials())>
            <#if displayRequiredFields>
                <div class="${properties.kcContentWrapperClass!}">
                    <div class="${properties.kcLabelWrapperClass!} subtitle">
                        <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                    </div>
                    <div class="col-md-10">
                        <h1 id="kc-page-title"><#nested "header"></h1>
                    </div>
                </div>
            <#else>
                <h1 id="kc-page-title"><#nested "header"></h1>
            </#if>
        <#else>
            <#if displayRequiredFields>
                <div class="${properties.kcContentWrapperClass!}">
                    <div class="${properties.kcLabelWrapperClass!} subtitle">
                        <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                    </div>
                    <div class="col-md-10">
                        <#nested "show-username">
                        <div id="kc-username" class="${properties.kcFormGroupClass!}">
                            <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                            <a id="reset-login" href="${url.loginRestartFlowUrl}" aria-label="${msg("restartLoginTooltip")}">
                                <div class="kc-login-tooltip">
                                    <i class="${properties.kcResetFlowIcon!}"></i>
                                    <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <#else>
                <#nested "show-username">
                <div id="kc-username" class="${properties.kcFormGroupClass!}">
                    <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                    <a id="reset-login" href="${url.loginRestartFlowUrl}" aria-label="${msg("restartLoginTooltip")}">
                        <div class="kc-login-tooltip">
                            <i class="${properties.kcResetFlowIcon!}"></i>
                            <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                        </div>
                    </a>
                </div>
            </#if>
        </#if>
      </header>
      <div id="kc-content">
        <div id="kc-content-wrapper">

          <#-- App-initiated actions should not see warning messages about the need to complete the action -->
          <#-- during login.                                                                               -->
          <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
              <div class="alert-${message.type} ${properties.kcAlertClass!} pf-m-<#if message.type = 'error'>danger<#else>${message.type}</#if>">
                  <div class="pf-c-alert__icon">
                      <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                      <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                      <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                      <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                  </div>
                      <span class="${properties.kcAlertTitleClass!}">${kcSanitize(message.summary)?no_esc}</span>
              </div>
          </#if>

          <#nested "form">

          <#if auth?has_content && auth.showTryAnotherWayLink()>
              <form id="kc-select-try-another-way-form" action="${url.loginAction}" method="post">
                  <div class="${properties.kcFormGroupClass!}">
                      <input type="hidden" name="tryAnotherWay" value="on"/>
                      <a href="#" id="try-another-way"
                         onclick="document.forms['kc-select-try-another-way-form'].submit();return false;">${msg("doTryAnotherWay")}</a>
                  </div>
              </form>
          </#if>

          <#nested "socialProviders">

          <#if displayInfo>
              <div id="kc-info" class="${properties.kcSignUpClass!}">
                  <div id="kc-info-wrapper" class="${properties.kcInfoAreaWrapperClass!}">
                      <#nested "info">
                  </div>
              </div>
          </#if>
        </div>
      </div>

    </div>
  </div>
  <footer id="kc-footer" class="${properties.kcFooterClass!}">
  <div class="row footer__links">
    <div class="col footer__col">
      <div class="footer__title">${msg("LINCS_footerLabelContact")}</div>
      <ul id="contact__items" class="footer__items clean-list">
        <li id="lincs__feedback" class="footer__item">
          <a
            href="${msg("LINCS_footerLinkURLFeedback")}" 
            target="_blank"
            rel="noopener noreferrer"
            class="footer__link-item"
            >${msg("LINCS_footerLinkFeedback")}</a>
        </li>
        <li id="lincs__email" class="footer__item">
          <a
            href="mailto://lincs@uoguelph.ca"
            target="_blank"
            rel="noopener noreferrer"
            class="footer__link-item"
            >${msg("LINCS_footerLinkEmail")}</a>
        </li>
    </div>
    <div class="col footer__col">
      <div class="footer__title">${msg("LINCS_footerLabelCommunity")}</div>
      <ul id="community__items" class="footer__items clean-list">
        <li id="lincs__newsletter-signup" class="footer__item">
          <a
            href="${msg("LINCS_footerLinkURLNewsletterSignup")}"
            target="_blank"
            rel="noopener noreferrer"
            class="footer__link-item"
            >${msg("LINCS_footerLinkNewsletterSignup")}</a>
        </li>
        <li id="lincs__newletter" class="footer__item">
          <a
            href=${msg("LINCS_footerLinkURLNewsletter")} 
            target="_blank"
            rel="noopener noreferrer"
            class="footer__link-item"
            >${msg("LINCS_footerLinkNewsletter")}</a>
        </li>
        <li class="footer__item">
          <div class="social-icon">
            <a
              href="https://www.twitter.com/lincsproject"
              target="_blank"
              rel="noreferrer noopener"
              aria-label="Twitter link"
            >
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--! Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"/></svg>
            </a>
          </div>
        
          <div class="social-icon">
            <a
              href="https://www.youtube.com/@lincsproject"
              target="_blank"
              rel="noreferrer noopener"
              aria-label="Youtube link"
            >
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!--! Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"/></svg>
            </a>
          </div>

          <div class="social-icon">
            <a
              href="https://gitlab.com/calincs"
              target="_blank"
              rel="noreferrer noopener"
              aria-label="GitLab link"
            >
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M503.5 204.6L502.8 202.8L433.1 21C431.7 17.5 429.2 14.4 425.9 12.4C423.5 10.8 420.8 9.9 417.9 9.6C415 9.3 412.2 9.7 409.5 10.7C406.8 11.7 404.4 13.3 402.4 15.5C400.5 17.6 399.1 20.1 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.1 111.5 17.6 109.6 15.5C107.6 13.4 105.2 11.7 102.5 10.7C99.9 9.7 97 9.3 94.1 9.6C91.3 9.9 88.5 10.8 86.1 12.4C82.8 14.4 80.3 17.5 78.9 21L9.3 202.8L8.5 204.6C-1.5 230.8-2.7 259.6 5 286.6C12.8 313.5 29.1 337.3 51.5 354.2L51.7 354.4L52.3 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"/></svg>
            </a>
          </div>
        </li>
      </ul>
    </div>
    <div class="col footer__col">
      <div class="footer__title">${msg("LINCS_footerLabelPolicies")}</div>
      <ul class="footer__items clean-list">
        <li class="footer__item">
          <a
            class="footer__link-item"
            href="${msg("LINCS_footerLinkURLPrivacyPolicy")}"
            >${msg("LINCS_footerLinkPrivacyPolicy")}</a
          >
        </li>
        <li class="footer__item">
          <a
            class="footer__link-item"
            href="${msg("LINCS_footerLinkURLTermsConditions")}"
            >${msg("LINCS_footerLinkTermsConditions")}</a
          >
        </li>
      </ul>
    </div>
    <div class="col footer__col">
      <div class="footer__title">${msg("LINCS_footerLabelPartners")}</div>
      <ul id="partners__items" class="footer__items clean-list">
        <div>
          <li class="footer__item">
            <a
              class="footer__link-item"
              href="${msg("LINCS_footerLinkURLFundingPartners")}"
              >${msg("LINCS_footerLinkFundingPartners")}</a
            >
          </li>
          <li class="footer__item">
            <a
              class="footer__link-item"
              href="${msg("LINCS_footerLinkURLInfraPartners")}"
              >${msg("LINCS_footerLinkInfraPartners")}</a
            >
          </li>
        </div>
        <li id="innovation__item" class="footer__item">
          <div class="cfi-logo">
            <a
              href="${msg("LINCS_footerLinkURLInnovation")}"
              target="_blank"
              rel="noreferrer noopener"
              aria-label="CFI"
            >
              <?xml version="1.0" encoding="utf-8"?>
              <!-- Generator: Adobe Illustrator 26.2.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
              <svg width="128.49" height="31.72" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 793.7 220.3" style="enable-background:new 0 0 793.7 220.3;" xml:space="preserve">
              <g>
                <path fill="currentColor" d="M19.9,34.9v79.2h20.4V23.8h-9.3C24.9,23.8,19.9,28.8,19.9,34.9C19.9,34.9,19.9,34.9,19.9,34.9z"/>
                <path fill="currentColor" d="M112,34.8v43.4L71.3,23.9l-0.1-0.1H52.7v90.4h20V58l42.1,56l0.1,0.1H132V23.8h-9C116.9,23.8,112,28.7,112,34.8z"/>
                <path fill="currentColor" d="M222.7,42V23.8h-8.3c-6.4,0-11.7,5.2-11.7,11.7c0,0,0,0,0,0v42.7L162,23.9l-0.1-0.1h-18.5v90.4h20V58l42,56l0.1,0.1h17.1
                  V97.4C214.3,79.9,214.3,59.5,222.7,42L222.7,42z"/>
                <path fill="currentColor" d="M419.1,23.8h-22L374,86.2L351,23.8h-22.5l1.3,3.2c9.8,11.1,15.5,25.3,16.1,40.2l19,47.6h17.9l36.2-90.4L419.1,23.8z"/>
                <path fill="currentColor" d="M455.7,23.1H437l-38.3,90.5l-0.2,0.5h20.9l8.2-20.1h37.2l8.1,19.9l0.1,0.2h21.3l-38.4-90.8L455.7,23.1z M434.8,75.9
                  l11.3-27.6l11.3,27.6H434.8z"/>
                <path fill="currentColor" d="M476.2,31.4c0,6.4,5.2,11.6,11.6,11.6h15.5v71.4h20.4V43h27.2v-19h-74.7V31.4z"/>
                <path fill="currentColor" d="M568.7,23.8h-8.9v90.4h20.4V35.3C580.3,29,575.1,23.8,568.7,23.8C568.8,23.8,568.8,23.8,568.7,23.8z"/>
                <path fill="currentColor" d="M640.3,22.2c-26.9,0-48,20.5-48,46.7v0.2c0,26.5,20.5,46.5,47.7,46.5c26.9,0,48-20.5,48-46.7v-0.3
                  C688,42.2,667.5,22.2,640.3,22.2z M666.7,69.2c0,15.7-11.3,27.5-26.4,27.5c-15.2,0-26.6-11.9-26.6-27.8v-0.3
                  c0-15.7,11.3-27.5,26.4-27.5c15.2,0,26.6,11.9,26.6,27.8V69.2z"/>
                <path fill="currentColor" d="M763.4,23.8h-9v54.4l-40.7-54.2l-0.1-0.1H695v90.4h20V58l42.1,56l0.1,0.1h17.1V34.8C774.4,28.7,769.4,23.8,763.4,23.8z"/>
                <path fill="currentColor" d="M277.4,38.3c-0.3,0.2-0.6,0.3-1,0.5c3.9,0.3,7.7,0.9,11.4,2c14.3,4.6,22.7,19.8,23.4,33.4c1,20.2-13.8,37-35.6,37.5
                  c-12.3,0.4-24.2-4.6-32.6-13.6c-7.2-7.9-11.6-17.2-13.3-28.6c-0.1-0.3-0.4-0.5-0.7-0.5c-0.3,0-0.6,0.3-0.6,0.6
                  c-0.1,2.5,0.1,5,0.5,7.4c3.1,17.8,15.7,32.7,33.7,39.9c17.9,7.1,34.7,4.3,50.2-6.2c24.7-16.7,23.7-53.9-2.6-69.3
                  C301.3,36.5,287,33.7,277.4,38.3z"/>
                <path fill="currentColor" d="M286.3,99.6c0.3-0.2,0.6-0.3,1-0.5c-3.9-0.2-7.7-0.7-11.5-1.7c-14.4-4.2-23.2-19.3-24.1-32.9c-1.4-20.3,13-37.4,34.7-38.4
                  c12.3-0.7,24.3,4,32.9,12.8c7.4,7.7,12,17,14,28.2c0.1,0.3,0.4,0.5,0.7,0.5c0.3,0,0.6-0.3,0.6-0.6c0-2.5-0.2-5-0.6-7.4
                  c-3.6-17.8-16.5-32.4-34.6-39.1s-34.8-3.5-50,7.3c-24.3,17.2-22.4,54.5,4.3,69.2C262.4,102,276.7,104.4,286.3,99.6z"/>
                <g>
                  <path fill="currentColor" d="M19.7,151.3L19.7,151.3c0-7.1,5.3-12.8,12.8-12.8c4.6,0,7.4,1.5,9.7,3.8l-3.4,4c-1.9-1.7-3.8-2.8-6.3-2.8
                    c-4.1,0-7.1,3.4-7.1,7.7v0.1c0,4.2,2.9,7.7,7.1,7.7c2.8,0,4.5-1.1,6.5-2.9l3.4,3.5c-2.5,2.7-5.3,4.4-10.1,4.4
                    C25.1,164,19.7,158.4,19.7,151.3z"/>
                  <path fill="currentColor" d="M44,158.2L44,158.2c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1l-1.3-4.1
                    c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6v10.9h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C47,163.9,44,161.9,44,158.2z M56.3,156.9V156c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1c0,1.4,1.2,2.3,2.9,2.3
                    C54.6,160.2,56.3,158.9,56.3,156.9z"/>
                  <path fill="currentColor" d="M65,144.7h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8v10.5
                    H65V144.7z"/>
                  <path fill="currentColor" d="M84.6,158.2L84.6,158.2c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1
                    l-1.3-4.1c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6v10.9h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C87.5,163.9,84.6,161.9,84.6,158.2z M96.9,156.9V156c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1c0,1.4,1.2,2.3,2.9,2.3
                    C95.2,160.2,96.9,158.9,96.9,156.9z"/>
                  <path fill="currentColor" d="M104.6,154.2L104.6,154.2c0-6.4,4.1-9.8,8.6-9.8c2.8,0,4.6,1.3,5.9,2.8v-9.3h5.3v25.7H119v-2.7c-1.3,1.8-3.1,3.1-5.9,3.1
                    C108.8,163.9,104.6,160.5,104.6,154.2z M119.1,154.2L119.1,154.2c0-3.2-2.1-5.3-4.6-5.3s-4.6,2-4.6,5.2v0.1c0,3.1,2.1,5.2,4.6,5.2
                    S119.1,157.3,119.1,154.2z"/>
                  <path fill="currentColor" d="M127,158.2L127,158.2c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1l-1.3-4.1
                    c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6v10.9h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C129.9,163.9,127,161.9,127,158.2z M139.3,156.9V156c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1c0,1.4,1.2,2.3,2.9,2.3
                    C137.6,160.2,139.3,158.9,139.3,156.9z"/>
                  <path fill="currentColor" d="M157.9,139h18.7v4.9h-13.3v5.2h11.7v4.9h-11.7v9.5h-5.4V139z"/>
                  <path fill="currentColor" d="M177.6,154.3L177.6,154.3c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C181.9,164,177.6,159.7,177.6,154.3z M192.7,154.3L192.7,154.3c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C190.9,159.4,192.7,157,192.7,154.3z"/>
                  <path fill="currentColor" d="M200.5,156.9v-12.2h5.3v10.5c0,2.5,1.2,3.8,3.2,3.8c2,0,3.3-1.3,3.3-3.8v-10.5h5.3v18.8h-5.3v-2.7c-1.2,1.6-2.8,3-5.5,3
                    C202.9,163.9,200.5,161.3,200.5,156.9z"/>
                  <path fill="currentColor" d="M221.5,144.7h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8
                    v10.5h-5.3V144.7z"/>
                  <path fill="currentColor" d="M241.5,154.2L241.5,154.2c0-6.4,4.1-9.8,8.6-9.8c2.8,0,4.6,1.3,5.9,2.8v-9.3h5.3v25.7h-5.3v-2.7c-1.3,1.8-3.1,3.1-5.9,3.1
                    C245.7,163.9,241.5,160.5,241.5,154.2z M256,154.2L256,154.2c0-3.2-2.1-5.3-4.6-5.3s-4.6,2-4.6,5.2v0.1c0,3.1,2.1,5.2,4.6,5.2
                    S256,157.3,256,154.2z"/>
                  <path fill="currentColor" d="M263.9,158.2L263.9,158.2c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1
                    l-1.3-4.1c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6v10.9h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C266.8,163.9,263.9,161.9,263.9,158.2z M276.2,156.9V156c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1
                    c0,1.4,1.2,2.3,2.9,2.3C274.5,160.2,276.2,158.9,276.2,156.9z"/>
                  <path fill="currentColor" d="M285.4,158.2v-8.9h-2.2v-4.6h2.2v-4.8h5.3v4.8h4.4v4.6h-4.4v8c0,1.2,0.5,1.8,1.7,1.8c1,0,1.9-0.2,2.6-0.7v4.3
                    c-1.1,0.7-2.4,1.1-4.2,1.1C287.6,163.9,285.4,162.6,285.4,158.2z"/>
                  <path fill="currentColor" d="M298.1,137.9h5.6v4.7h-5.6V137.9z M298.3,144.7h5.3v18.8h-5.3V144.7z"/>
                  <path fill="currentColor" d="M306.3,154.3L306.3,154.3c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C310.7,164,306.3,159.7,306.3,154.3z M321.5,154.3L321.5,154.3c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C319.6,159.4,321.5,157,321.5,154.3z"/>
                  <path fill="currentColor" d="M329.5,144.7h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8
                    v10.5h-5.3V144.7z"/>
                  <path fill="currentColor" d="M21.1,184.7h-2.2v-4.4h2.2v-1.2c0-2.1,0.5-3.6,1.5-4.6c1-1,2.4-1.5,4.3-1.5c1.7,0,2.8,0.2,3.8,0.5v4.4
                    c-0.8-0.3-1.5-0.5-2.4-0.5c-1.2,0-1.9,0.6-1.9,2v0.7h4.3v4.4h-4.3v14.3h-5.3V184.7z"/>
                  <path fill="currentColor" d="M31.4,189.6L31.4,189.6c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C35.7,199.4,31.4,195,31.4,189.6z M46.5,189.6L46.5,189.6c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C44.7,194.8,46.5,192.4,46.5,189.6z"/>
                  <path fill="currentColor" d="M54.5,180.1h5.3v3.8c1.1-2.6,2.8-4.3,6-4.1v5.6h-0.3c-3.5,0-5.7,2.1-5.7,6.6v7h-5.3V180.1z"/>
                  <path fill="currentColor" d="M78.4,174.3h5.4v24.6h-5.4V174.3z"/>
                  <path fill="currentColor" d="M88.2,180.1h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8v10.5
                    h-5.3V180.1z"/>
                  <path fill="currentColor" d="M109.1,180.1h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8
                    v10.5h-5.3V180.1z"/>
                  <path fill="currentColor" d="M129,189.6L129,189.6c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C133.3,199.4,129,195,129,189.6z M144.1,189.6L144.1,189.6c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C142.2,194.8,144.1,192.4,144.1,189.6z"/>
                  <path fill="currentColor" d="M149.4,180.1h5.7l4.2,12.6l4.3-12.6h5.6l-7.4,19h-4.8L149.4,180.1z"/>
                  <path fill="currentColor" d="M169.2,193.5L169.2,193.5c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1
                    l-1.3-4.1c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6v10.9h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C172.1,199.3,169.2,197.3,169.2,193.5z M181.5,192.3v-0.9c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1
                    c0,1.4,1.2,2.3,2.9,2.3C179.8,195.6,181.5,194.2,181.5,192.3z"/>
                  <path fill="currentColor" d="M190.7,193.6v-8.9h-2.2v-4.6h2.2v-4.8h5.3v4.8h4.4v4.6h-4.4v8c0,1.2,0.5,1.8,1.7,1.8c1,0,1.9-0.2,2.6-0.7v4.3
                    c-1.1,0.7-2.4,1.1-4.2,1.1C193,199.3,190.7,198,190.7,193.6z"/>
                  <path fill="currentColor" d="M203.5,173.3h5.6v4.7h-5.6V173.3z M203.6,180.1h5.3v18.8h-5.3V180.1z"/>
                  <path fill="currentColor" d="M211.7,189.6L211.7,189.6c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C216,199.4,211.7,195,211.7,189.6z M226.8,189.6L226.8,189.6c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C224.9,194.8,226.8,192.4,226.8,189.6z"/>
                  <path fill="currentColor" d="M234.8,180.1h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8
                    v10.5h-5.3V180.1z"/>
                </g>
                <g>
                  <path fill="currentColor" d="M402.5,138h18.7v4.9h-13.3v5.2h11.7v4.9h-11.7v9.5h-5.4V138z"/>
                  <path fill="currentColor" d="M422.2,153.3L422.2,153.3c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C426.5,163,422.2,158.7,422.2,153.3z M437.3,153.3L437.3,153.3c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C435.5,158.4,437.3,156.1,437.3,153.3z"/>
                  <path fill="currentColor" d="M445.3,143.8h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8
                    v10.5h-5.3V143.8z"/>
                  <path fill="currentColor" d="M465.3,153.2L465.3,153.2c0-6.4,4.1-9.8,8.6-9.8c2.8,0,4.6,1.3,5.9,2.8V137h5.3v25.7h-5.3v-2.7c-1.3,1.8-3.1,3.1-5.9,3.1
                    C469.5,163,465.3,159.5,465.3,153.2z M479.8,153.2L479.8,153.2c0-3.2-2.1-5.3-4.6-5.3s-4.6,2-4.6,5.2v0.1c0,3.1,2.1,5.2,4.6,5.2
                    S479.8,156.4,479.8,153.2z"/>
                  <path fill="currentColor" d="M487.7,157.2L487.7,157.2c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1
                    l-1.3-4.1c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6v10.9h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C490.6,163,487.7,160.9,487.7,157.2z M500,156V155c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1c0,1.4,1.2,2.3,2.9,2.3
                    C498.3,159.3,500,157.9,500,156z"/>
                  <path fill="currentColor" d="M509.2,157.3v-8.9H507v-4.6h2.2V139h5.3v4.8h4.4v4.6h-4.4v8c0,1.2,0.5,1.8,1.7,1.8c1,0,1.9-0.2,2.6-0.7v4.3
                    c-1.1,0.7-2.4,1.1-4.2,1.1C511.4,162.9,509.2,161.6,509.2,157.3z"/>
                  <path fill="currentColor" d="M521.9,137h5.6v4.7h-5.6V137z M522.1,143.8h5.3v18.8h-5.3V143.8z"/>
                  <path fill="currentColor" d="M530.1,153.3L530.1,153.3c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C534.5,163,530.1,158.7,530.1,153.3z M545.2,153.3L545.2,153.3c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C543.4,158.4,545.2,156.1,545.2,153.3z"/>
                  <path fill="currentColor" d="M553.2,143.8h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8
                    v10.5h-5.3V143.8z"/>
                  <path fill="currentColor" d="M582.7,153.3L582.7,153.3c0-5.4,4.1-9.9,9.9-9.9c3.5,0,5.8,1.2,7.5,3.2l-3.3,3.5c-1.2-1.3-2.4-2.1-4.3-2.1
                    c-2.7,0-4.6,2.4-4.6,5.1v0.1c0,2.9,1.9,5.2,4.8,5.2c1.8,0,3-0.8,4.3-2l3.1,3.2c-1.8,2-3.9,3.4-7.7,3.4
                    C586.8,163,582.7,158.7,582.7,153.3z"/>
                  <path fill="currentColor" d="M601.4,157.2L601.4,157.2c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1
                    l-1.3-4.1c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6v10.9h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C604.3,163,601.4,160.9,601.4,157.2z M613.6,156V155c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1c0,1.4,1.2,2.3,2.9,2.3
                    C611.9,159.3,613.6,157.9,613.6,156z"/>
                  <path fill="currentColor" d="M622.3,143.8h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8
                    v10.5h-5.3V143.8z"/>
                  <path fill="currentColor" d="M641.9,157.2L641.9,157.2c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1
                    l-1.3-4.1c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6v10.9h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C644.8,163,641.9,160.9,641.9,157.2z M654.2,156V155c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1c0,1.4,1.2,2.3,2.9,2.3
                    C652.5,159.3,654.2,157.9,654.2,156z"/>
                  <path fill="currentColor" d="M661.9,153.2L661.9,153.2c0-6.4,4.1-9.8,8.6-9.8c2.8,0,4.6,1.3,5.9,2.8V137h5.3v25.7h-5.3v-2.7c-1.3,1.8-3.1,3.1-5.9,3.1
                    C666.1,163,661.9,159.5,661.9,153.2z M676.4,153.2L676.4,153.2c0-3.2-2.1-5.3-4.6-5.3s-4.6,2-4.6,5.2v0.1c0,3.1,2.1,5.2,4.6,5.2
                    S676.4,156.4,676.4,153.2z"/>
                  <path fill="currentColor" d="M685.4,137h5.6v4.7h-5.6V137z M685.6,143.8h5.3v18.8h-5.3V143.8z"/>
                  <path fill="currentColor" d="M693.7,153.3L693.7,153.3c0-5.4,3.8-9.9,9.3-9.9c6.3,0,9.2,4.9,9.2,10.2c0,0.4,0,0.9-0.1,1.4H699c0.5,2.4,2.2,3.7,4.6,3.7
                    c1.8,0,3.1-0.6,4.6-1.9l3.1,2.7c-1.8,2.2-4.3,3.5-7.7,3.5C697.9,163,693.7,159.1,693.7,153.3z M707,151.7c-0.3-2.4-1.7-4-4-4
                    c-2.2,0-3.7,1.6-4.1,4H707z"/>
                  <path fill="currentColor" d="M715,143.8h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8v10.5
                    H715V143.8z"/>
                  <path fill="currentColor" d="M735.9,143.8h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7v12.2h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8
                    v10.5h-5.3V143.8z"/>
                  <path fill="currentColor" d="M755.7,153.3L755.7,153.3c0-5.4,3.8-9.9,9.3-9.9c6.3,0,9.2,4.9,9.2,10.2c0,0.4,0,0.9-0.1,1.4h-13.1
                    c0.5,2.4,2.2,3.7,4.6,3.7c1.8,0,3.1-0.6,4.6-1.9l3.1,2.7c-1.8,2.2-4.3,3.5-7.7,3.5C759.9,163,755.7,159.1,755.7,153.3z M769,151.7
                    c-0.3-2.4-1.7-4-4-4c-2.2,0-3.7,1.6-4.1,4H769z"/>
                  <path fill="currentColor" d="M401.9,179.1h5.3v2.7c1.3-1.8,3.1-3.1,5.9-3.1c4.4,0,8.6,3.4,8.6,9.7v0.1c0,6.3-4.1,9.7-8.6,9.7c-2.8,0-4.6-1.3-5.9-2.8
                    v8.1h-5.3V179.1z M416.4,188.6L416.4,188.6c0-3.2-2.1-5.3-4.6-5.3c-2.5,0-4.6,2.1-4.6,5.2v0.1c0,3.1,2.1,5.2,4.6,5.2
                    C414.3,193.8,416.4,191.8,416.4,188.6z"/>
                  <path fill="currentColor" d="M423.6,188.7L423.6,188.7c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C427.9,198.4,423.6,194.1,423.6,188.7z M438.7,188.7L438.7,188.7c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C436.9,193.8,438.7,191.4,438.7,188.7z"/>
                  <path fill="currentColor" d="M446.5,191.3v-12.2h5.3v10.5c0,2.5,1.2,3.8,3.2,3.8s3.3-1.3,3.3-3.8v-10.5h5.3V198h-5.3v-2.7c-1.2,1.6-2.8,3-5.5,3
                    C448.8,198.3,446.5,195.7,446.5,191.3z"/>
                  <path fill="currentColor" d="M467.5,179.1h5.3v3.8c1.1-2.6,2.8-4.3,6-4.1v5.6h-0.3c-3.5,0-5.7,2.1-5.7,6.6v7h-5.3V179.1z"/>
                  <path fill="currentColor" d="M490.8,172.3h5.3V198h-5.3V172.3z"/>
                  <path fill="currentColor" d="M499,182.5c2.4-0.2,3.6-1.5,3.4-3.5h-2.2v-5.6h5.7v4.7c0,4.6-2.3,6.5-6.5,6.6L499,182.5z"/>
                  <path fill="currentColor" d="M509.1,172.3h5.6v4.7h-5.6V172.3z M509.3,179.1h5.3V198h-5.3V179.1z"/>
                  <path fill="currentColor" d="M518.4,179.1h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7V198h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8V198
                    h-5.3V179.1z"/>
                  <path fill="currentColor" d="M539.3,179.1h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7V198h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8V198
                    h-5.3V179.1z"/>
                  <path fill="currentColor" d="M559.2,188.7L559.2,188.7c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C563.5,198.4,559.2,194.1,559.2,188.7z M574.3,188.7L574.3,188.7c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C572.4,193.8,574.3,191.4,574.3,188.7z"/>
                  <path fill="currentColor" d="M579.5,179.1h5.7l4.2,12.6l4.3-12.6h5.6l-7.4,19H587L579.5,179.1z"/>
                  <path fill="currentColor" d="M599.4,192.6L599.4,192.6c0-4.2,3.1-6.1,7.6-6.1c1.9,0,3.3,0.3,4.6,0.8v-0.3c0-2.2-1.4-3.4-4-3.4c-2,0-3.5,0.4-5.2,1
                    l-1.3-4.1c2.1-0.9,4.1-1.5,7.3-1.5c2.9,0,5,0.8,6.4,2.1c1.4,1.4,2,3.5,2,6V198h-5.2v-2c-1.3,1.4-3.1,2.4-5.7,2.4
                    C602.3,198.3,599.4,196.3,599.4,192.6z M611.7,191.3v-0.9c-0.9-0.4-2.1-0.7-3.4-0.7c-2.3,0-3.7,0.9-3.7,2.6v0.1
                    c0,1.4,1.2,2.3,2.9,2.3C610,194.6,611.7,193.3,611.7,191.3z"/>
                  <path fill="currentColor" d="M620.9,192.6v-8.9h-2.2v-4.6h2.2v-4.8h5.3v4.8h4.4v4.6h-4.4v8c0,1.2,0.5,1.8,1.7,1.8c1,0,1.9-0.2,2.6-0.7v4.3
                    c-1.1,0.7-2.4,1.1-4.2,1.1C623.1,198.3,620.9,197,620.9,192.6z"/>
                  <path fill="currentColor" d="M633.6,172.3h5.6v4.7h-5.6V172.3z M633.8,179.1h5.3V198h-5.3V179.1z"/>
                  <path fill="currentColor" d="M641.8,188.7L641.8,188.7c0-5.5,4.4-9.9,10.2-9.9c5.8,0,10.2,4.3,10.2,9.7v0.1c0,5.4-4.4,9.8-10.2,9.8
                    C646.2,198.4,641.8,194.1,641.8,188.7z M657,188.7L657,188.7c0-2.8-2-5.3-5-5.3c-3.1,0-4.9,2.4-4.9,5.1v0.1c0,2.8,2,5.2,5,5.2
                    C655.1,193.8,657,191.4,657,188.7z"/>
                  <path fill="currentColor" d="M665,179.1h5.3v2.7c1.2-1.6,2.8-3,5.5-3c4,0,6.4,2.7,6.4,7V198h-5.3v-10.5c0-2.5-1.2-3.8-3.2-3.8s-3.3,1.3-3.3,3.8V198
                    H665V179.1z"/>
                </g>
              </g>
              </svg>
            </a>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <div class="land-acknowledgement">
    <p>
      <em>${msg("LINCS_footerLandAcknowledgement")}</em>
    </p>
  </div>
</footer>

</body>
</html>
</#macro>
