# Keycloak for LINCS

This repository contains custom theming and realm configuration exports of our Keycloak environments (dev, stage, and prod). It also has CI/CD configured to install Keycloak with our customisations into these environments.

The deployment jobs have to be triggered manually from the Pipelines menu. It is recommended to do a backup of the Keycloak database before attempting a deployment.

## Customising the theme

Reference: <https://www.keycloak.org/docs/latest/server_development/#_themes>

## Testing locally

There is a Docker Compose file that starts up a Keycloak instance in dev mode and maps the LINCS theme to the container.

```bash
docker compose up
```

Then go to <http://localhost:8080> and log into Keycloak with admin/admin and set the realm login theme to `lincs`.

## Backing up Keycloak

The realm with all clients can be exported via the frontend but the export won't contain any of the client secrets. One can manually update the export file with the current secrets and that import will work as expected. However, Keycloak users can't be exported and imported with the Keycloak UI. One can export the entire real, including users, by following the offline export guide here: <https://www.keycloak.org/server/importExport>

Currently, we backup Keycloak by backing up the Postgres database and storing it in S3 storage.
